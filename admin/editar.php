<?php
session_start();
include('../php/conexao.php');
//include('../php/timeout.php');
/*    Author: Abdiel Pereira Cordeiro
  Tela: Interação Adm
 */
?>
<html>
    <head>
        <meta charset="UTF-8"/>
        <title>Interacao</title>
        <link rel="stylesheet"  type="text/css" href="css/cssEditar.css">
        <script type="text/javascript" src="js/javascript.js"></script>
        <script  type="text/javascript" src="../bootstrap/js/popper.min.js" crossorigin="anonymous"></script>
        <script type="text/javascript" src="../bootstrap/js/jquery.min.js"></script>
        <script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css" crossorigin="anonymous"> 
        <script type="text/javascript" src="../bootstrap/js/jquery-2.1.3.min.js"></script>
    </head>
    <body style="background-color:white; background:none; overflow:hidden;">
        <div class="row ">
            <div class="col-12 corpo">
                <?php
                $id = $_GET['id'];
                $_SESSION['idUser'] = $id;
                $queryUser = file_get_contents("../php/sql/selectEditar.sql");
                $queryUser .= " WHERE ID_CLIENTE = '{$id}';";
                $conect = mysqli_query($conn, $queryUser);
                $resultado = mysqli_fetch_assoc($conect);
                ?>
                <form action="../php/upUser.php" method="post">
                    <div class="row l2">
                        <div class="col-6">
                            <label class="names">Nome da Empresa: </label>
                        </div>
                        <div class="col-4 txt1">
                            <?php
                            echo "<input name=\"nomeEmpresa\" placeholder=\"{$resultado['NOME_EMPRESA']}\" >"
                            ?>
                        </div>
                    </div>
                    <div class="row l2">
                        <div class="col-6">
                            <label class="names">Nome: </label>
                        </div>
                        <div class="col-4 txt1">
                            <?php
                            echo "<input name=\"NOME_CLIENTE\" placeholder=\"{$resultado['NOME_CLIENTE']}\" >"
                            ?>
                        </div>
                    </div>
                    <div class="row l2">
                        <div class="col-6">
                            <label class="names">Nivel de Acesso: </label>
                        </div>
                        <div class="col-3 txt1">
                            <?php
                            if ($resultado['NIVELACESSO'] == 'admin') {
                                $acesso = 'Administrador';
                            } else if ($resultado['NIVELACESSO'] == 'master') {
                                $acesso = 'Master';
                            } else if ($resultado['NIVELACESSO'] == 'usuario') {
                                $acesso = 'Usuário';
                            } else if ($resultado['NIVELACESSO'] == 'cliente') {
                                $acesso = 'Cliente';
                            }
                            echo "<input name=\"nivelAcesso\" placeholder=\"{$acesso}\" >"
                            ?>
                        </div>
                    </div>
                    <div class="row l2">
                        <div class="col-4">
                            <label class="names">E-mail: </label>
                        </div>
                        <div class="col-3 txt1">
                            <?php
                            echo "<input id=\"emailUser\" name=\"emailCliente\" placeholder=\"{$resultado['EMAIL_CLIENTE']}\" >"
                            ?>
                        </div>
                    </div>
                    <div class="row l2">
                        <div class="col-2">
                            <label class="names">ID: </label>
                        </div>
                        <div class="col-2">
                            <?php
                            echo "{$resultado['ID_CLIENTE']}"
                            ?>
                        </div>
                        <div class="col-4">
                            <label class="names">Status: </label>
                        </div>
                        <div class="col-1 txt1">
                            <?php
                            if ($resultado['STATUS_CLIENTE'] == 'A') {
                                $status = 'Ativo';
                            } else if ($resultado['STATUS_CLIENTE'] == 'I') {
                                $status = 'Inativo';
                            }
                            echo "$status"
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-4">
                            <label class="names">ID Empresa: </label>
                        </div>
                        <div class="col-2">
                            <?php
                            echo "{$resultado['ID_EMPRESA']}"
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-8"></div>
                        <div class="col-4">
                            <input type="submit" id="salvarInfos" value="Salvar">
                        </div>
                    </div>
                </form>
                <form action="../php/addResp.php" method="post">
                    <div class="row l2">
                        <div class="col-11">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="inputGroupSelect01">Setor:</label>
                                </div>
                                <select name="responsavel" class="custom-select" id="inputGroupSelect01">
                                    <option value="" disabled selected>Selecione</option>
                                    <?php
                                    include('../php/conexao.php');
                                    $queryCliente = "SELECT* FROM consultResponsavel";
                                    if ($result = $conn->query($queryCliente)) {
                                        while ($row = $result->fetch_assoc()) {
                                            echo "<option>{$row['NOME_EMPRESA']}</option>";
                                        }
                                        $result->close();
                                    }
                                    $addResp = $_POST['SETOR'];
                                    $_SESSION['nameUSer'] = $addResp;
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-1 bntADD">
                            <button type="submit" class="btn btn-primary addResp"> </button>
                        </div>
                    </div>
                </form>
                <div class="row l3"></div>
                <div class="row l2">
                    <?php
                    $queryresp = file_get_contents("../php/sql/selectResponsavelDist.sql");
                    $queryresp = " WHERE SETOR.ID_CLIENTE = '{$id}' ";
                    if ($conect1 = $conn->query($queryresp)) {
                        while ($row = $conect1->fetch_assoc()) {
                            echo""
                            . "<div class=\"col-6\">"
                            . "<label>{$row['NOME_SETOR']}</label>"
                            . "</div>"
                            . "<div class=\"col-2\">"
                            . "<td><a id=\"deleteResp\" name=\"delete\" href='../php/deleteResp.php?id={$row['ID_SETOR']}'><img src='../imagem/inativar.png'></a></td>"
                            . "</div>";
                        }
                    }
                    ?>
                </div>
                <div class="row l3">
                    <div class="col-12">
                        <?php
                        if (isset($_SESSION['msgResp'])) {
                            echo"<div class=\"balaoErro\">";
                            echo $_SESSION['msgResp'];
                            unset($_SESSION['msgResp']);
                            echo"</div>";
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </body> 
</html>
