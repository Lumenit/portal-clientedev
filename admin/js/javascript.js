$(document).ready(function () {

    $('a[name=modal]').click(function (e) {
        e.preventDefault();

        var id = $(this).attr('href');

        var maskHeight = $(document).height();
        var maskWidth = $(window).width();

        $('#mask').css({'width': maskWidth, 'height': maskHeight});

        $('#mask').fadeIn(1000);
        $('#mask').fadeTo("slow", 0.8);

        //Get the window height and width
        var winH = $(window).height();
        var winW = $(window).width();

        $(id).css('top', winH / 2 - $(id).height() / 2);
        $(id).css('left', winW / 2 - $(id).width() / 2);

        $(id).fadeIn(2000);

    });

    $('.window .close').click(function (e) {
        e.preventDefault();

        $('#mask').hide();
        $('.window').hide();
    });
    $('.window .closeUser').click(function (e) {
        e.preventDefault();

        $('#mask').hide();
        $('.window').hide();
    });
    $('#mask').click(function () {
        $(this).hide();
        $('.window').hide();
    });

});

function validaSenha(input) {
    if (input.value !== document.getElementById('novaSenha').value) {
        input.setCustomValidity('Repita a senha corretamente');
    } else {
        input.setCustomValidity('');
    }
}
function makeFileList() {
    var input = document.getElementById("arquivo");
    var ul = document.getElementById("resultado");
    var ull = document.getElementById("resultado2");
    var ul2 = document.getElementById("resultado1");
    var conta = 0;

    while (ul.hasChildNodes()) {
        ul.removeChild(ul.firstChild);
        ull.removeChild(ull.firstChild);
        ul2.removeChild(ul2.firstChild);
    }
    for (var i = 0; i < input.files.length; i++) {
        var li = document.createElement("li");
        var lii = document.createElement("lii");
        li.innerHTML = input.files[i].name;

        if (input.files[i].size > 1073741824) {
            var conta = (conta + (((input.files[i].size / 1024) / 1024) / 1024));
        } else {
            var conta = (conta + ((input.files[i].size / 1024) / 1024));
        }


        ul.appendChild(li);
        ull.appendChild(lii);
    }
    lii.innerHTML = parseFloat(conta.toFixed(1));

    var li2 = document.createElement("li");
    var li3 = document.createElement("lii");

    li2.innerHTML = i + " Arquivo selecionado";
    if (conta > 1) {
        li3.innerHTML = i + "Gb";
    } else {
        li3.innerHTML = i + "Mb";
    }


    ul2.appendChild(li2);
    ull.appendChild(li3);

    if (!ul.hasChildNodes()) {
        var li = document.createElement("li");
        li.innerHTML = 'Escolha';

        ul.appendChild(li);
    }
}

function duplicarCampos() {
    var clone = document.getElementById('origem').cloneNode(true);
    var destino = document.getElementById('destino');
    destino.appendChild(clone);

    var camposClonados = clone.getElementsByTagName('input');

    for (i = 0; i < camposClonados.length; i++) {
        camposClonados[i].value = '';
    }
}

function removerCampos(id) {
    var node1 = document.getElementById('destino');
    node1.removeChild(node1.childNodes[0]);
}

function openEditaUser(id) {
    location.href = '#openModalEditaUser';
    document.getElementById("imodal").setAttribute('src', "/dev.portal/admin/editar.php?id=" + id);
}

function chPage() {
    var id = $('#inputGroupSelect01').val();
    $('#seg').load("/dev.portal/admin/buscarNome.php?id=" + id);
}

function carregarTSenha() {
    $("#janelSenha").load('/dev.portal/janelas/trocarsenha.php');
}
function carregarUsuario() {
    $("#janelUsuario").load('/dev.portal/janelas/newUser.php');
}
function carregarEmpresa() {
    $("#janelUsuario").load('/dev.portal/janelas/newCompany.php');
}
function chRela() {
    var modulo = $('#selectModulo').val();
    var dataInic = $('#dataIni').val();
    var dataFim = $('#dataFim').val();
    $('#seg').load("/dev.portal/janelas/procModulos.php?modulo=" + modulo + '&dataInic=' + dataInic + '&dataFim=' + dataFim);
}


function bxArquivo2() {
    var arr = [];
    var link = document.createElement("a");
    var url = "192.168.2.198/dev.portal/anexoPortal/";
    $("input:checkbox[name=arquivoB]:checked").each(function () {
        arr.push($(this).val());
        link.download = arr;
        link.href = url;
        link.click();
    });
}

function bxArquivo() {
    var arr = [];
    $("input:checkbox[name=arquivoB]:checked").each(function () {
        arr.push($(this).val());
    });
    $('#teste').load("/dev.portal/php/baixarMassa.php?nomes=" + arr);
}

function download(uri, nome) {
    var link = document.createElement("a");
    link.download = nome;
    link.href = uri;
    link.click();
}