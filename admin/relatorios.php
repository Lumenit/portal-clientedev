<?php
session_start();
include('../php/conexao.php');
include('../php/timeout.php');
/*
  Author: Abdiel Pereira Cordeiro
  Tela: Administrador
 */
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Portal Cliente</title>
        <link rel="stylesheet"  type="text/css" href="css/cssConfiguracoes.css">
        <link rel="stylesheet"  type="text/css" href="css/function.css">
        <script type="text/javascript" src="js/javascript.js"></script>
        <link rel="shortcut icon" type="imagem/png" href="../imagem/iconPortal.png">
        <script  type="text/javascript" src="../bootstrap/js/popper.min.js" crossorigin="anonymous"></script>
        <script type="text/javascript" src="../bootstrap/js/jquery.min.js"></script>
        <script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css" crossorigin="anonymous"> 
        <script type="text/javascript" src="../bootstrap/js/jquery-2.1.3.min.js"></script>
    </head>
    <body>
        <div class="row linhaCabecario"></div>
        <div class="row cabecario">
            <div class="col-2 p1">
                <a href="home.php" ><img src="../imagem/lumen.png" id="logoLumen"></a>
            </div>
            <div class="col-3"></div>
            <div class="col-2">
                <div class="row l10"></div>
                <div class="row">
                    <div class="col-2">
                        <img src="../imagem/Usuario.png" id="iconeUsuario"/>
                    </div>
                    <div class="col-10">
                        <div id="infosUser"><?php echo $_SESSION['nameCliente'] ?></div>
                    </div>
                </div>
            </div>
            <div class="col-2"></div>
            <div class="col-2">
                <div class="row l5"></div>
                <div class="row">
                    <div class="btn-group" role="group" aria-label="Menu Bar">
                        <div class="btn-group" role="group">
                            <button id="btnGroupDrop1" type="button" class="btn btnSecondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Menu
                            </button>
                            <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                <a class="btn dropdown-item" href="home.php">Home</a>
                                <?php
                                if ($_SESSION['acesso'] == 'admin' OR $_SESSION['acesso'] == 'master') {
                                    echo"<a class=\"dropdown-item\" href=\"relatorios.php\">Configurações</a>";
                                }
                                ?>
                                <a onclick="carregarTSenha();" class="btn dropdown-item" data-toggle="modal" data-target="#modalSenha" href="#modalSenha" >Trocar Senha</a>
                            </div>
                            <form method="post" action="../php/logout.php" name="Sair" >
                                <button id="sair" type="submit" class="btn btn-secondary">Sair</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-1">
                <div class="row l11"></div>
                <div class="row">
                    <div class="col-12">
                        <label id="versao"><?php echo"{$_SESSION['versaoSistem']}"; ?></label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="modal fade" id="modalSenha" tabindex="-1" role="dialog" aria-labelledby="modalSenha" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div id="janelSenha"></div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="modalUsuario" tabindex="-1" role="dialog" aria-labelledby="modalUsuario" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div id="janelUsuario"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row l1"></div>
        <div class="row corpo">
            <div class="col-1">
                <div id="erroTrocaSenha">
                    <?php
                    if (isset($_SESSION['msg'])) {
                        echo"<div class=\"balaoErro\">";
                        echo $_SESSION['msg'];
                        unset($_SESSION['msg']);
                        echo"</div>";
                    }
                    ?>
                </div>
            </div>
            <div class="col-2" id="menuLateral">
                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" >
                    <a class="nav-link active linhasMenu" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Usuários</a>
                    <a class="nav-link linhasMenu" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Relatórios</a>
                    <a class="nav-link linhasMenu" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">Messages</a>
                    <!--<a class="nav-link linhasMenu" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false">Setting</a>-->
                </div>
            </div>
            <div class="col-7" id="backgroundTela">
                <div class="tab-content" id="v-pills-tabContent">
                    <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                        <div class="row l2">
                            <div class="col-3">
                                <?php if ($_SESSION['acesso'] == 'master') { ?>
                                    <a onclick="carregarUsuario();" class="btn" data-toggle="modal" data-target="#modalUsuario" href="#modalUsuario" ><img src="../imagem/addUser.png"></a>
                                    <a onclick="carregarEmpresa();" class="btn" data-toggle="modal" data-target="#modalUsuario" href="#modalUsuario" ><img src="../imagem/addCompany.png"></a>
                                <?php } ?>
                            </div>
                            <div class="col-2"></div>
                            <div class="col-5 txt2">
                                <label class="t1">Lista de Usuários</label>
                            </div>
                        </div>
                        <div class="row grade">
                            <div class="col-12 tbl2">
                                <table class="table tmTabelas">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th scope="col">NOME</th>
                                            <th scope="col">EMPRESA</th>
                                            <th scope="col">EMAIL</th>
                                            <th scope="col">STATUS</th>
                                            <th scope="col">EDITAR</th>
                                            <th scope="col">ATIVAR</th>
                                            <th scope="col">INATIVAR</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $selectCliente = "SELECT* FROM consultaUser";
                                        if ($result = $conn->query($selectCliente)) {
                                            while ($row = $result->fetch_assoc()) {
                                                $idCliente = $row['ID_CLIENTE'];
                                                $stausCliente = '';
                                                if ($row['STATUS_CLIENTE'] == 'A') {
                                                    $stausCliente = "<img src='../imagem/aberto.png'>";
                                                } else {
                                                    $stausCliente = "<img src='../imagem/fechado.png'>";
                                                }
                                                echo "<tr>";
                                                echo "<td>{$row['NOME_CLIENTE']}</td>";
                                                echo "<td id=\"tdEmpresa\">{$row['NOME_EMPRESA']}</td>";
                                                echo "<td>{$row['EMAIL_CLIENTE']}</td>";
                                                echo "<td class=\"iconL\">$stausCliente</td>";
                                                echo "<td class=\"iconL\"><a onclick=\"openEditaUser('{$row['ID_CLIENTE']}')\"><img src='../imagem/editar.png'></a></td>";
                                                echo "<td class=\"iconL\"><a name=\"ativar\" id=\"iconEditaUser\" href='../php/ativar.php?id={$row['ID_CLIENTE']}'><img src='../imagem/ativar.png'></a></td>";
                                                echo "<td class=\"iconL\"><a name=\"inativar\" id=\"iconEditaUser\" href='../php/inativar.php?id={$row['ID_CLIENTE']}'><img src='../imagem/inativar.png'></a></td>";
                                                echo "</tr>";
                                            }
                                            $result->close();
                                        }
                                        ?>
                                    </tbody>
                                </table>
                                <div id="openModalEditaUser" class="modalDialogEditaUser">
                                    <div style="background:white;">
                                        <a href="#close" title="Close" class="close">X</a>
                                        <iframe src='' style='width:100%;height:100%; border:none;' id='imodal'></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                        <div class="row l5"></div>
                        <div class="row">
                            <div class="col-3">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">Inicio</span>
                                    </div>
                                    <input type="date" id="dataIni" name="dataInic" class="form-control" aria-label="Data" required="required">
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">Fim</span>
                                    </div>
                                    <input type="date" name="dataFim" id="dataFim" class="form-control" aria-label="Data" required="required">
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <label class="input-group-text" for="inputGroupSelect01">Tipos</label>
                                    </div>
                                    <select id="selectModulo" name="OpcoesRelatorios" class="custom-select OpcoesCliente" required="required">
                                        <option value="" disabled selected>Selecione</option>
                                        <option value="DELETE">Deletados</option>
                                        <option value="HISTORICO">Histórico</option>
                                        <option value="NEWUSER">Novo Usuário</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-1">
                                <button type="submit" class="btn btnRelat" onclick="chRela();"></button>
                            </div>
                        </div>
                        <div class="row grade">
                            <div class="col-12 tbl2">
                                <div id="seg"></div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">3</div>
                    <!--<div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">4</div>-->
                </div>
            </div>
        </div>
    </body>
</html>
