<?php
include('../php/conexao.php');

$dataInicio = $_GET['dataInic'];
$dataFim = $_GET['dataFim'];
$modulo = $_GET['modulo'];
if ($modulo == 'DELETE') {
    ?>
    <table class="table tmTabelas">
        <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nome</th>
                <th scope="col">QNT</th>
                <th scope="col">DATA</th>
                <th scope="col">ANEXO</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $selectExclusao = "SELECT* FROM consultExclusao";
            $selectExclusao .= " WHERE DATA_EXCLUSAO BETWEEN '{$dataInicio}' AND '{$dataFim}'";
            if ($result = $conn->query($selectExclusao)) {
                while ($row = $result->fetch_assoc()) {
                    echo "<tr>";
                    echo "<td class=\"exTb\">{$row['ID_EXCLUSAO']}</td>";
                    echo "<td>{$row['NOME_CLIENTE']}</td>";
                    echo "<td class=\"exTb\">{$row['QNT_ANEXO']}</td>";
                    echo "<td>{$row['DATA_EXCLUSAO']}</td>";
                    echo "<td>{$row['NOME_ANEXO']}</td>";
                    echo "</tr>";
                }
                $result->close();
            }
            ?>
        </tbody>
    </table>
    <?php
} else if ($modulo == 'HISTORICO') {
    ?>
    <table class="table tmTabelas">
        <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">ID_EVENTO</th>
                <th scope="col">NOME</th>
                <th scope="col">DATA</th>
                <th scope="col">NOME EVENTO</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $selectHistorico = file_get_contents("../php/sql/selectHistorico.sql");
            $selectHistorico .= "WHERE DATA_HISTORICO BETWEEN '{$dataInicio}' AND '{$dataFim}'";
            if ($result = $conn->query($selectHistorico)) {
                while ($row = $result->fetch_assoc()) {
                    echo "<tr>";
                    echo "<td class=\"exTb\">{$row['ID_HISTORICO']}</td>";
                    echo "<td class=\"exTb\">{$row['ID_EVENTO']}</td>";
                    echo "<td class=\"exTb\">{$row['NOME_CLIENTE']}</td>";
                    echo "<td>{$row['DATA_HISTORICO']}</td>";
                    echo "<td>{$row['NOME_EVENTO']}</td>";
                    echo "</tr>";
                }
                $result->close();
            }
            ?>
        </tbody>
    </table>
    <?php
} else if ($modulo == 'NEWUSER') {
    ?>
    <table class="table tmTabelas">
        <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">ID_EVENTO</th>
                <th scope="col">NOME</th>
                <th scope="col">DATA</th>
                <th scope="col">NOME EVENTO</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $selectHistorico = "SELECT* FROM consulHistonewuser";
            $selectHistorico .= " WHERE DATA_HISTORICO BETWEEN '{$dataInicio}' AND '{$dataFim}'";
            if ($result = $conn->query($selectHistorico)) {
                while ($row = $result->fetch_assoc()) {
                    echo "<tr>";
                    echo "<td class=\"exTb\">{$row['ID_HISTORICO']}</td>";
                    echo "<td class=\"exTb\">{$row['NOME_CLIENTE']}</td>";
                    echo "<td class=\"exTb\">{$row['DATA_HISTORICO']}</td>";
                    echo "<td>{$row['ID_CLIENTE']}</td>";
                    echo "</tr>";
                }
                $result->close();
            }
            ?>
        </tbody>
    </table>
    <?php
}

