<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Trocar senha</title>
        <script  type="text/javascript" src="../bootstrap/js/popper.min.js" crossorigin="anonymous"></script>
        <script type="text/javascript" src="../bootstrap/js/jquery.min.js"></script>
        <script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css" crossorigin="anonymous"> 
        <script type="text/javascript" src="../bootstrap/js/jquery-2.1.3.min.js"></script>
    </head>
    <body>
        <form method="post" action="../php/newCompany.php">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Nova Empresa</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-1"></div>
                    <div class="input-group mb-3 col-10">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Nome</span>
                        </div>
                        <input name="nomeEmpresa" id="nomeUser" type="text" class="form-control" placeholder="Nome Empresa" aria-label="Username"  required="required" aria-describedby="basic-addon1">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                <button type="submit" class="btn btn-primary">Salvar Empresa</button>
            </div>
        </form>
    </body>
</html>
