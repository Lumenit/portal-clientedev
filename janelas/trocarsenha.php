<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Trocar senha</title>
        <script  type="text/javascript" src="../bootstrap/js/popper.min.js" crossorigin="anonymous"></script>
        <script type="text/javascript" src="../bootstrap/js/jquery.min.js"></script>
        <script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css" crossorigin="anonymous"> 
        <script type="text/javascript" src="../bootstrap/js/jquery-2.1.3.min.js"></script>
    </head>
    <body>
        <form method="post" action="../php/trocarSenha.php">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Trocar Senha</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">Senha Atual</span>
                            </div>
                            <input type="password" name="senhaAtual" class="form-control" placeholder="Senha atual" aria-label="Senha Atual" aria-describedby="basic-addon1" required="required">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">Nova Senha</span>
                            </div>
                            <input type="password" name="senhaNova" class="form-control" id="novaSenha" placeholder="Nova Senha" aria-label="Nova senha" aria-describedby="basic-addon1" required="required" pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$" title="Preencha o campo com letras Maiúsculas, minúsculas e números">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">Confirmar Senha</span>
                            </div>
                            <input type="password" name="validarSenha" class="form-control" id="novaSenhaConfri" placeholder="Confirmar Senha" aria-label="Confirma Senha" aria-describedby="basic-addon1" required="required" oninput="validaSenha(this)">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" title="Confirma" class="btn btn-primary">Confirmar</button>
            </div>
        </form>
    </body>
</html>
