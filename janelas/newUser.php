<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Trocar senha</title>
        <script  type="text/javascript" src="../bootstrap/js/popper.min.js" crossorigin="anonymous"></script>
        <script type="text/javascript" src="../bootstrap/js/jquery.min.js"></script>
        <script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css" crossorigin="anonymous"> 
        <script type="text/javascript" src="../bootstrap/js/jquery-2.1.3.min.js"></script>
    </head>
    <body>
        <form method="post" action="../php/newUser.php">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Novo Usuário</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-1"></div>
                    <div class="input-group mb-3 col-10">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Nome Cliente</span>
                        </div>
                        <input name="NovoUserName" id="nomeUser" type="text" class="form-control" placeholder="Nome Cliente" aria-label="Username"  required="required" aria-describedby="basic-addon1">
                    </div>
                    <div class="col-1"></div>
                </div>
                <div class="row">
                    <div class="col-1"></div>
                    <div class="col-10">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="inputGroupSelect01">Nome Empresa</label>
                            </div>
                            <select name="empresa" class="custom-select" id="inputGroupSelect01">
                                <option value="" disabled selected>Selecione</option>
                                <?php
                                include('../php/conexao.php');
                                $selectResp = file_get_contents("../php/sql/selectEmpresa.sql");
                                if ($result = $conn->query($selectResp)) {
                                    while ($row = $result->fetch_assoc()) {
                                        echo "<option value=\"{$row['ID_EMPRESA']}\">{$row['NOME_EMPRESA']}</option>";
                                    }
                                    $result->close();
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-1"></div>
                </div>
                <div class="row">
                    <div class="col-1"></div>
                    <div class="col-10">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">Email</span>
                            </div>
                            <input name="NovoUserEmail" id="emailUser" required="required" type="text" class="form-control" placeholder="Email" aria-label="Email" aria-describedby="basic-addon1">
                        </div>
                    </div>
                    <div class="col-1"></div>
                </div>
                <div class="row">
                    <div class="col-1"></div>
                    <div class="col-10">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="inputGroupSelect01">Nivel de Acesso</label>
                            </div>
                            <select name="OpcoesAcesso" class="custom-select" id="inputGroupSelect01">
                                <option selected>Acesso</option>
                                <option value="master">master</option>
                                <option value="admin">admin</option>
                                <option value="usuario">usuario</option>
                                <option value="cliente">cliente</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-1"></div>
                </div>
                <div class="row" id="origem">
                    <div class="col-1"></div>
                    <div class="col-10 l8">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="inputGroupSelect01">SETOR</label>
                            </div>
                            <select name="responsavel[]" class="custom-select" id="inputGroupSelect01">
                                <option value="" disabled selected>Selecione</option>
                                <?php
                                include('../php/conexao.php');
                                $selectResp = "SELECT* FROM consultResponsavel";
                                if ($result = $conn->query($selectResp)) {
                                    while ($row = $result->fetch_assoc()) {
                                        echo "<option>{$row['NOME_EMPRESA']}</option>";
                                    }
                                    $result->close();
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-9"></div>
                    <div class="col-3">
                        <img  src="../imagem/add.png" style="cursor: pointer" onclick="duplicarCampos()">
                        <img  src="../imagem/sub.png" style="cursor: pointer" onclick="removerCampos(this)">
                    </div>
                </div>
                <div class="row">   
                    <div class="col-10" id="destino"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                <button type="submit" class="btn btn-primary">Salvar Usuário</button>
            </div>
        </form>
    </body>
</html>
