<?php

session_start();
/*
  Autor: Abdiel Cordeiro

 */

include('../php/conexao.php');
date_default_timezone_set('America/Sao_Paulo');
$date = date('Y-m-d H:i');

$idCliente = $_GET['id'];


$inativar = "UPDATE CLIENTE SET STATUS_CLIENTE = 'I' WHERE ID_CLIENTE = '{$idCliente}';";
$updateStatus = mysqli_query($conn, $inativar);

$queryHistorico = file_get_contents("sql/insertHistorico.sql");
$queryHistorico .= " VALUES('{$_SESSION['idCliente']}','{$idCliente}','{$date}','INATIVAR');";
$insertHistorico = mysqli_query($conn, $queryHistorico);

$_SESSION['msnUser'] = "Usuário inativo com sucesso!!!";
header("Location: ../admin/relatorios.php");

