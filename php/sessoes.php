<?php session_start() ?>
<?php

/**
 * Author:  Abdiel Pereira Cordeiro
 * Created: 28/06/2019
 */
include('conexao.php');

$email = $_POST['email'];

$select = "SELECT* FROM consultaSessao";
$select .= " WHERE EMAIL_CLIENTE = '{$email}'";
$infos = mysqli_query($conn, $select);
$resultado = mysqli_fetch_assoc($infos);
$extensoes = array(".doc", ".pdf", ".docx", ".rar", ".zip", ".xlsx"
    , ".xls", ".exe", ".ppt", ".pptx",".csv");
if (isset($_SESSION)) {
    $_SESSION['idCliente'] = $resultado['ID_CLIENTE'];
    $_SESSION['nomeEmpresa'] = $resultado['NOME_EMPRESA'];
    $_SESSION['idEmpresa'] = $resultado['ID_EMPRESA'];
    $_SESSION['nameCliente'] = $resultado['NOME_CLIENTE'];
    $_SESSION['emailUsuario'] = $email;
    $_SESSION['acesso'] = $resultado['NIVELACESSO'];
    $_SESSION['ext'] = $extensoes;
    $_SESSION['versaoSistem'] = "v1.9.5";
    $_SESSION['tamanhoArquivo'] = 1073741824;
}