<?php
/*
  Author     : Abdiel Pereira Cordeiro
  Tela: Administrador Portal
 */
session_start();
include('conexao.php');
include('conectFTP.php');

date_default_timezone_set('America/Sao_Paulo');
$date = date('Y-m-d H:i');
$idAnexo = $_GET['id'];

$arq = "SELECT* FROM ANEXO WHERE ID_ANEXO = '{$idAnexo}';";
$conect1 = mysqli_query($conn, $arq);
$nomeAnex = mysqli_fetch_assoc($conect1);
$nomeAnexo = $nomeAnex['NOME_ANEXO'];

//$resulta = $conn->query($arq) or die($conn->error);

$pasta = "/var/www/html/dev.portal/anexoPortal/" . $nomeAnexo;

if (@ftp_delete($conexao_ftp, $pasta)) {
    $queryExclusao = file_get_contents("sql/insertExclusao.sql");
    $queryExclusao .= " VALUES ('{$_SESSION['idCliente']}','{$date}','1');";
    $conect = mysqli_query($conn, $queryExclusao);
    $id = mysqli_insert_id($conn);
    
    //$resultal = $conn->query($queryExclusao) or die($conn->error);
    $queryaux = "INSERT INTO AUX_EXCLUSAO(ID_EXCLUSAO,NOME_ANEXO) VALUES('{$id}','{$nomeAnexo}')";
    $conect1 = mysqli_query($conn, $queryaux);

    $queryHistorico = file_get_contents("sql/insertHistorico.sql");
    $queryHistorico .= " VALUES('{$_SESSION['idCliente']}','{$idAnexo}','{$date}','DELETE');";
    $insertHistorico = mysqli_query($conn, $queryHistorico);

    $querydeleteAnexo = "DELETE FROM ANEXO WHERE ID_ANEXO = '{$idAnexo}';";
    $conect2 = mysqli_query($conn, $querydeleteAnexo);


    if ($_SESSION['acesso'] == 'cliente') {
        $_SESSION['msg'] = 'O arquivo: ' . $nomeAnexo . ' Foi deletado com sucesso!!!';
        header('Location: ../user/home.php');
    } else {
        $_SESSION['msg'] = 'O arquivo: ' . $nomeAnexo . ' Foi deletado com sucesso!!!';
        header('Location: ../admin/home.php');
    }
} else {
    if ($_SESSION['acesso'] == 'cliente') {
        $_SESSION['msg'] = 'Erro para apagar este arquivo!!';
        header('Location: ../user/home.php');
    } else {
        $_SESSION['msg'] = 'Erro para apagar este arquivo!!';
        header('Location: ../admin/home.php');
    }
}