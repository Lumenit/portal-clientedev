<?php

session_start();
/*
  Author     : Abdiel Pereira Cordeiro
 */
date_default_timezone_set('America/Sao_Paulo');
$date = date('Y-m-d H:i');
header('Content-Type: text/html; charset=UTF-8');
include('conexao.php');

$email = mysqli_real_escape_string($conn, $_POST['emailTrocarSenha']);
$valUserQuery = file_get_contents("sql/selectCliente.sql");
$valUserQuery .= " WHERE EMAIL_CLIENTE = '{$email}'";
$conect = mysqli_query($conn, $valUserQuery);
$veriUser = mysqli_fetch_assoc($conect);
$row = mysqli_num_rows($conect);

if ($row == 1) {

    function gerar_senha($tamanho, $maiusculas, $minusculas, $numeros, $simbolos) {
        $ma = "ABCDEFGHIJKLMNOPQRSTUVYXWZ"; // $ma contem as letras maiúsculas
        $mi = "abcdefghijklmnopqrstuvyxwz"; // $mi contem as letras minusculas
        $nu = "0123456789"; // $nu contem os números
        $si = "!@#$&+*"; // $si contem os símbolos

        $senha = 0;

        if ($maiusculas) {
            // se $maiusculas for "true", a variável $ma é embaralhada e adicionada para a variável $senha
            $senha .= str_shuffle($ma);
        }

        if ($minusculas) {
            // se $minusculas for "true", a variável $mi é embaralhada e adicionada para a variável $senha
            $senha .= str_shuffle($mi);
        }

        if ($numeros) {
            // se $numeros for "true", a variável $nu é embaralhada e adicionada para a variável $senha
            $senha .= str_shuffle($nu);
        }

        if ($simbolos) {
            // se $simbolos for "true", a variável $si é embaralhada e adicionada para a variável $senha
            $senha .= str_shuffle($si);
        }

        // retorna a senha embaralhada com "str_shuffle" com o tamanho definido pela variável $tamanho
        return substr(str_shuffle($senha), 0, $tamanho);
    }

    $novaSenha = gerar_senha(6, true, true, true, true);
    $CriptSenha = password_hash($novaSenha, PASSWORD_DEFAULT);
    $query = "UPDATE CLIENTE SET SENHA_CLIENTE = '{$CriptSenha}' WHERE EMAIL_CLIENTE = '{$email}'";
    $result = mysqli_query($conn, $query);

    $nomeCliente = $veriUser['NOME_CLIENTE'];
    include('email.php');

    $para = $email;
    $buffer = file_get_contents("email/emailSenha.php");
    $tags = array($nomeCliente, $email, $date, $novaSenha);
    $troca = array('#nomeCliente#', '#emailCliente#', '#dataModificacao#', '#novaSenha#');
    $envio = str_replace($troca, $tags, $buffer);
    $assunto = "Nova de Senha";

    if (smtpmailer($para, $nomeCliente, $de, $de_nome, $assunto, $envio)) {
        $_SESSION['msg'] = "Senha alterada com sucesso!!!";
        header('Location: ../index.php');
    }
} else if ($row == 0) {
    $_SESSION['msg'] = "Usuário não existe!!!";
    header('Location: ../index.php');
}