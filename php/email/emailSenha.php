<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Nova senha</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>
    <body style="margin: 0; padding: 0;">
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
            <td align="center" style="padding: 40px 0 30px 0;">
                <img src="http://177.207.191.190:8082/imagem/lumen.png" alt="Logo Lumen" width="200" height="50" style="display: block;" />
            </td>
            <tr>
                <td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;">
                                <b>Nova Senha</b>
                            </td>
                        </tr>
                        <tr>
                            <td  style="height: 20px;">

                            </td>
                        </tr>
                    </table>
                    <br>
                    <div style="width: 100%;border-bottom: 1px solid #000000;"></div>
                    <br>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td width="2" valign="top">
                                            &nbsp;&nbsp; <b>Nome:</b>
                                        </td>
                                        <td style="font-size: 0; line-height: 0;" width="1">
                                            &nbsp;
                                        </td>
                                        <td width="180" valign="top">
                                            #nomeCliente#
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr style="height: 10px;"></tr>
                        <tr>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td width="2" valign="top">
                                            &nbsp;&nbsp; <b>Email:</b>
                                        </td>
                                        <td style="font-size: 0; line-height: 0;" width="1">
                                            &nbsp;
                                        </td>
                                        <td width="180" valign="top">
                                            #emailCliente#
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr style="height: 10px;"></tr>
                        <tr>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td width="2" valign="top">
                                            &nbsp;&nbsp; <b>Data alteração:</b>
                                        </td>
                                        <td style="font-size: 0; line-height: 0;" width="1">
                                            &nbsp;
                                        </td>
                                        <td width="180" valign="top">
                                            #dataModificacao#
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr style="height: 10px;"></tr>
                        <tr>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td width="2" valign="top">
                                            &nbsp;&nbsp; <b>Senha Nova:</b>
                                        </td>
                                        <td style="font-size: 0; line-height: 0;" width="1">
                                            &nbsp;
                                        </td>
                                        <td width="180" valign="top">
                                            #novaSenha#
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <br>
                    <div style="width: 100%;border-bottom: 1px solid #000000;"></div>
                    <br>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td width="50" valign="top">
                                            <img src="http://177.207.191.190:8082/imagem/alert.png" alt="Aviso" width="28" height="28" style="display: block;" border="0" />
                                        </td>
                                        <td width="500" valign="top"> 
                                            <b>Este e-mail é gerado automaticamente.</b>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td bgcolor="#B0C4DE"  style="padding: 30px 30px 30px 30px;">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <td align="right">
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="color: black; font-family: Arial, sans-serif; font-size: 12px;">
                                        Portal Clientes © 2019 - Todos os direitos reservados.
                                    </td>
                                    <td style="width: 10px;"></td>
                                    <td>
                                        <a href="http://bit.ly/InstaLumenIT" title="Instagram">
                                            <img src="http://177.207.191.190:8082/imagem/ICONE-INSTA-LARANJA.png" alt="Instagram" width="38" height="38" style="display: block;" border="0" />
                                        </a>
                                    </td>
                                    <td style="font-size: 0; line-height: 0;" width="20">&nbsp;</td>
                                    <td>
                                        <a href="http://bit.ly/FacebookLumenIT" title="Facebook">
                                            <img src="http://177.207.191.190:8082/imagem/ICONE-FACE-LARANJA.png" alt="Facebook" width="38" height="38" style="display: block;" border="0" />
                                        </a>
                                    </td>
                                    <td style="font-size: 0; line-height: 0;" width="20">&nbsp;</td>
                                    <td>
                                        <a href="http://bit.ly/LinkedinLumenIT" title="Linkedin">
                                            <img src="http://177.207.191.190:8082/imagem/ICONE-LINKEDIN-LARANJA.png" alt="Linkedin" width="38" height="38" style="display: block;" border="0" />
                                        </a>
                                    </td>
                                    <td style="font-size: 0; line-height: 0;" width="20">&nbsp;</td>
                                    <td>
                                        <a href="http://www.portal.lumenit.com" title="Portal Clientes">
                                            <img src="http://177.207.191.190:8082/imagem/icone-portal-2.png" alt="Portal-Clientes" width="38" height="38" style="display: block;" border="0" />
                                        </a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>