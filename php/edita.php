<?php

session_start();
include('conexao.php');
date_default_timezone_set('America/Sao_Paulo');
$date = date('Y-m-d H:i');
$cont = 0;

if (isset($_POST['email'])) {
    $novoEmail = $_POST['email'];
    $editarEmail = "UPDATE CLIENTE SET EMAIL_CLIENTE = '{$novoEmail}' WHERE ID_CLIENTE = '{$idCliente}';";
    $updateStatus = mysqli_query($conn, $editarEmail);
    $cont = $cont + 1;
} else if (isset($_POST['nomeEmpresa'])) {
    $novoEmpresa = $_POST['nomeEmpresa'];
    $editarNome = "UPDATE CLIENTE SET NOME_EMPRESA = '{$novoEmpresa}' WHERE ID_CLIENTE = '{$idCliente}';";
    $updateStatus = mysqli_query($conn, $editarNome);
    $cont = $cont + 1;
} else if (isset($_POST['NOME_CLIENTE'])) {
    $novoCliente = $_POST['NOME_CLIENTE'];
    $editarNome = "UPDATE CLIENTE SET NOME_CLIENTE = '{$novoCliente}' WHERE ID_CLIENTE = '{$idCliente}';";
    $updateStatus = mysqli_query($conn, $editarNome);
    $cont = $cont + 1;
}

if ($cont > 0) {
    $queryHistorico = file_get_contents("sql/insertHistorico.sql");
    $queryHistorico .= " VALUES('{$_SESSION['idCliente']}','{$idCliente}','{$date}','EDITAR');";
    $insertHistorico = mysqli_query($conn, $queryHistorico);
}