<?php

include('conexao.php');
date_default_timezone_set('America/Sao_Paulo');
$date = date('Y-m-d H:i');

/*
  Author     : Abdiel Pereira Cordeiro
  Portal
 */

if (isset($_SESSION['ultimoClick']) AND ! empty($_SESSION['ultimoClick'])) {
    $tempoAtual = time();
    if (($tempoAtual - $_SESSION['ultimoClick']) > '1000') {
        $queryHistorico = "INSERT INTO HISTORICO(ID_CLIENTE,DATA_HISTORICO,NOME_EVENTO) VALUES('{$_SESSION['idCliente']}','{$date}','LOGOUT');";
        $insertHistorico = mysqli_query($conn, $queryHistorico);

        header("Location:../php/logout.php?motivo=inatividade");
        exit();
    } else {
        $_SESSION['ultimoClick'] = time();
    }
} else {
    $_SESSION['ultimoClick'] = time();
}
