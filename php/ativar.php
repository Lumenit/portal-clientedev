<?php
/*
  Author     : Abdiel Pereira Cordeiro
  Portal Cliente
 */
session_start();
include('../php/conexao.php');
date_default_timezone_set('America/Sao_Paulo');
$date = date('Y-m-d H:i');

$idCliente = $_GET['id'];

$ativar = "UPDATE CLIENTE SET STATUS_CLIENTE = 'A' WHERE ID_CLIENTE = '{$idCliente}';";
$updateStatus = mysqli_query($conn, $ativar);
$queryHistorico = file_get_contents("sql/insertHistorico.sql");
$queryHistorico .= " VALUES('{$_SESSION['idCliente']}','{$idCliente}','{$date}','ATIVAR');";
$insertHistorico = mysqli_query($conn, $queryHistorico);

$_SESSION['msnUser'] = "Usuário ativo com sucesso!!!";
header("Location: ../admin/relatorios.php");

