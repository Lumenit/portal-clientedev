<?php

session_start();
/*
  Author     : Abdiel Pereira Cordeiro
  Portal Cliente Cliente
 */

include('conexao.php');
include('conectFTP.php');
/*
 * Configuração 
 */
date_default_timezone_set('America/Sao_Paulo');
$date = date('Y-m-d H:i');

$contAnexo = count($_FILES["arquivo"]["name"]);
$tamanhoMaximo = $_SESSION['tamanhoArquivo'];
$extensoes = $_SESSION['ext'];
$caminho = "/var/www/html/dev.portal/anexoPortal/";
/* ------------------------------------- */

$idCliente = $_SESSION['idCliente'];
$para2 = $_SESSION['emailUsuario'];
$descricao = $_POST['assunto'];
$assuntoInsert = $_POST['titulo'];

/*
 * Select destinatario 
 */
$para = $_POST['OpcoesCliente'];
$qNomeEmpresa = file_get_contents("sql/selectDestinatarioUser.sql");
$qNomeEmpresa .= " WHERE CLIENTE.EMAIL_CLIENTE = '{$para}';";
$resultado = mysqli_query($conn, $qNomeEmpresa);
$rowEmpresa = mysqli_fetch_assoc($resultado);
$myName = $rowEmpresa['ID_EMPRESA'];

$SelectnomeEmpresa = "SELECT* FROM EMPRESA WHERE ID_EMPRESA = '{$myName}'";
$nomeEmpresaResul = mysqli_query($conn, $SelectnomeEmpresa);
$resultadoEmpresa = mysqli_fetch_assoc($nomeEmpresaResul);
$nomeEmpresa = $resultadoEmpresa['NOME_EMPRESA'];
/* ------------------------------------- */

$testeEnvio = false;
for ($i = 0; $i < $contAnexo; $i++) {

    $nomeArquivo = $_FILES["arquivo"]["name"][$i];
    $tamanhoArquivo = $_FILES["arquivo"]["size"][$i];
    $nomeTemporario = $_FILES["arquivo"]["tmp_name"][$i];

    if (isset($nomeArquivo)) {
        $_SESSION['msg'] = false;

        if (!in_array(strrchr($nomeArquivo, "."), $extensoes)) {
            $_SESSION['msg'] = "A extensão do arquivo <b>" . $nomeArquivo . "</b><br> não é válida";
            header("Location: ../user/home.php");
        }
        if ($tamanhoArquivo > $tamanhoMaximo) {
            $_SESSION['msg'] = "O(s) arquivos excedem o tamanho permitido.";
            header("Location: ../user/home.php");
        }
        if (isset($_SESSION['msg'])) {

            $path_parts = pathinfo($_FILES["arquivo"]["name"][$i]);
            $extension = $path_parts['extension'];
            $destino = $caminho . $nomeArquivo;
            if (ftp_put($conexao_ftp, $destino, $nomeTemporario, FTP_BINARY)) {
                $queryInserir = file_get_contents("sql/insertAnexo.sql");
                echo "ID Empresa Remetente: " . $_SESSION['idEmpresa'];
                $queryInserir .= " VALUES('{$myName}','{$nomeArquivo}','{$_SESSION['idEmpresa']}','{$date}','{$descricao}','{$assuntoInsert}')";
                $inserirUp = mysqli_query($conn, $queryInserir);
                $idAnexo = mysqli_insert_id($conn);

                $queryHistorico = file_get_contents("sql/insertHistorico.sql");
                $queryHistorico .= " VALUES('{$idCliente}','{$idAnexo}','{$date}','UPLOAD');";
                $inserirHistorico = mysqli_query($conn, $queryHistorico);
                $testeEnvio = true;
            } else {
                $_SESSION['msg'] = "Erro para salvar o arquivo\nTente Novamente!!!";
                header('Location: ../user/home.php');
            }
        } else {
            header("Location: ../user/home.php");
        }
    } else {
        $_SESSION['msg'] = "" . "<br/>";
        ftp_close($conexao_ftp);
        header("Location: ../user/home.php");
    }
}
if ($testeEnvio == true) {

    include('email.php');

    $buffer = file_get_contents("email/enviarAnexo.php");
    $tags = array($idAnexo, $date, $nomeEmpresa, $rowEmpresa['NOME_CLIENTE'], $descricao, $contAnexo);
    $troca = array('#idAnexo#', '#date#', '#nomeEmpresa#', '#nomeCliente#', ' #descricao#', '#quantAnexo#');
    $envio = str_replace($troca, $tags, $buffer);
    $assunto = "Novo Arquivo";
    $nomeCliente = $rowEmpresa['NOME_CLIENTE'];

    if (smtpmailer($para, $nomeCliente, $de, $de_nome, $assunto, $envio)) {
        $_SESSION['msg'] = "O arquivo <b>" . $nomeArquivo . "</b><br> foi enviado com sucesso.";
        header('Location: ../user/home.php');
    } else {
        $_SESSION['msg'] = "Erro ao enviar email";
        header('Location: ../user/home.php');
    }
}
