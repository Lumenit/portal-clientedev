<?php
session_start();
/*
  Author: Abdiel Pereira Cordeiro
 */

include('conexao.php');
date_default_timezone_set('America/Sao_Paulo');
$date = date('Y-m-d H:i');

$queryHistorico = "INSERT INTO HISTORICO(ID_CLIENTE,DATA_HISTORICO,NOME_EVENTO) VALUES('{$_SESSION['idCliente']}','{$date}','LOGOUT');";
$insertHistorico = mysqli_query($conn, $queryHistorico);


session_destroy();
header("Location: ../index.php");
exit;
