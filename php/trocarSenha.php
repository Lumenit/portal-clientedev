<?php session_start(); ?>
<?php

/*
  Author     : Abdiel Pereira Cordeiro
 */
date_default_timezone_set('America/Sao_Paulo');
$date = date('Y-m-d H:i');
include('conexao.php');

$senhaAtual = $_POST['senhaAtual'];
$novaSenha = $_POST['senhaNova'];
$confSenha = $_POST['validarSenha'];
$id = $_SESSION['idCliente'];
$email = $_SESSION['emailUsuario'];
$para = $email;

$query = file_get_contents("sql/selectCliente.sql");
$query .= " WHERE ID_CLIENTE = '{$id}'";
$result = mysqli_query($conn, $query);
$resultado = mysqli_fetch_assoc($result);
$senhaBanco = password_verify($senhaAtual, $resultado['SENHA_CLIENTE']);
//$resulta = $conn->query($query) or die($conn->error); 
$nomeCliente = $resultado['NOME_CLIENTE'];

if ($senhaBanco == true) {
    $CriptSenha = password_hash($confSenha, PASSWORD_DEFAULT);
    $querySenha = "UPDATE CLIENTE SET SENHA_CLIENTE = '{$CriptSenha}' WHERE ID_CLIENTE = '{$id}'";
    $uplaod = mysqli_query($conn, $querySenha);

    include('email.php');

    $buffer = file_get_contents("email/emailSenha.php");
    $tags = array($nomeCliente, $email, $date, $novaSenha);
    $troca = array('#nomeCliente#', '#emailCliente#', '#dataModificacao#', '#novaSenha#');
    $envio = str_replace($troca, $tags, $buffer);
    $assunto = "Troca de Senha";

    if (smtpmailer($para, $nomeCliente, $de, $de_nome, $assunto, $envio)) {
        if ($resultado['NIVELACESSO'] == 'admin' || $resultado['NIVELACESSO'] == 'usuario' || $resultado['NIVELACESSO'] == 'master') {
            $_SESSION['msg'] = "Senha alterada com sucesso!!!";
            header('Location: ../admin/home.php');
        } else if ($resultado['NIVELACESSO'] == 'cliente') {
            $_SESSION['msg'] = "Senha alterada com sucesso!!!";
            header('Location: ../user/home.php');
        }
    }
} else {
    if ($resultado['NIVELACESSO'] == 'admin' || $resultado['NIVELACESSO'] == 'usuario' || $resultado['NIVELACESSO'] == 'master') {
        $_SESSION['msg'] = "Senha digitada não condiz com a senha atual!!!";
        header('Location: ../admin/home.php');
    } else if ($resultado['NIVELACESSO'] == 'cliente') {
        $_SESSION['msg'] = "Senha digitada não condiz com a senha atual!!!";
        header('Location: ../user/home.php');
    }
}