<?php
/*
  Author     : Abdiel Pereira Cordeiro
  Portal Cliente
 */
include('conexao.php');
date_default_timezone_set('America/Sao_Paulo');
$date = date('Y-m-d H:i');

if (empty($_POST['email']) || empty($_POST['senha'])) {
    header('Location: ../index.php');
    exit();
}
$email = $_POST['email'];
$senha = $_POST['senha'];
$minhaSenha = $senha;
//echo password_hash($senha, PASSWORD_DEFAULT);

$queryCliente = file_get_contents("sql/selectCliente.sql");
$queryCliente .= " WHERE EMAIL_CLIENTE = '{$email}'";
$result = mysqli_query($conn, $queryCliente);
$veriUser = mysqli_fetch_assoc($result);
$row = mysqli_num_rows($result);

if ($row == 0) {
    session_start();
    $_SESSION['msg'] = "Usuário não existe!!!";
    header('Location: ../index.php');
} else {
    //$resulta = $conn->query($queryAcesso) or die($conn->error);
    if (!isset($_SESSION)) {
        include('sessoes.php');
        if (password_verify($minhaSenha, $veriUser['SENHA_CLIENTE'])) {
            if ($veriUser['NIVELACESSO'] == 'admin' || $veriUser['NIVELACESSO'] == 'usuario' || $veriUser['NIVELACESSO'] == 'master') {
                $queryHistorico = file_get_contents("sql/insertHistorico.sql");
                $queryHistorico .= " VALUES('{$veriUser['ID_CLIENTE']}',NULL,'{$date}','LOGIN');";
                $enInsert = mysqli_query($conn, $queryHistorico);
                $_SESSION['user'] = $_SESSION['idCliente'];
                header('Location: ../admin/home.php');
            } else if ($resultado['NIVELACESSO'] == 'cliente') {
               $queryHistorico = file_get_contents("sql/insertHistorico.sql");
                $queryHistorico .= " VALUES('{$veriUser['ID_CLIENTE']}',NULL,'{$date}','LOGIN');";
                $enInsert = mysqli_query($conn, $queryHistorico);
                $_SESSION['user'] = $_SESSION['idCliente'];
                header('Location: ../user/home.php');
            } else {
                $_SESSION['msg'] = "Erro ao logar!!!";
                header('Location: ../index.php');
            }
        } else {
            $_SESSION['msg'] = "Senha incorreta!!!";
            header('Location: ../index.php');
        }
    } else {
        session_start();
        $_SESSION['msg'] = "Já existe um sessão aberta com este usuário!!!";
        header('Location: ../index.php');
    }
}
