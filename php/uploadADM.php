<?php

session_start();
/*
  Author     : Abdiel Pereira Cordeiro
  Portal Cliente Cliente ADM
 */

include('conexao.php');
include('conectFTP.php');


/*
 * Informações 
 */
date_default_timezone_set('America/Sao_Paulo');
$date = date('Y-m-d H:i');
$extensoes = $_SESSION['ext'];
$caminho = "/var/www/html/dev.portal/anexoPortal/";
$contAnexo = count($_FILES["arquivo"]['name']);
$tamanhoMaximo = $_SESSION['tamanhoArquivo'];

// -- > Infos Funcionario
$myEmpresa = $_SESSION['idEmpresa'];
$idFun = $_SESSION['idCliente'];

// -- > Infos Cliente
$idEmpresa = $_POST["OpcoesEmpresa"];
$SelectnomeEmpresa = "SELECT* FROM EMPRESA WHERE ID_EMPRESA = '{$idEmpresa}'";
$nomeEmpresaResul = mysqli_query($conn, $SelectnomeEmpresa);
$resultadoEmpresa = mysqli_fetch_assoc($nomeEmpresaResul);
$nomeEmpresa = $resultadoEmpresa['NOME_EMPRESA'];

$emailCliente = $_POST["OpcoesCliente"];
$queryCliente = file_get_contents("sql/selectCliente.sql");
$queryCliente .= " WHERE EMAIL_CLIENTE='{$emailCliente}';";
$resulCliente = mysqli_query($conn, $queryCliente);
$resultado = mysqli_fetch_assoc($resulCliente);

// -- > Email
$para2 = $_SESSION['emailUsuario'];
$para = $emailCliente;
$descricao = $_POST['assunto'];
$assuntoInsert = $_POST['titulo'];

/*
 * Função envia o Arquivo e dispara o Email
 */
$testeEnvio = false;
for ($i = 0; $i < $contAnexo; $i++) {
    $contTeste = + 1;
    $nomeArquivo = $_FILES["arquivo"]["name"][$i];
    $tamanhoArquivo = $_FILES["arquivo"]["size"][$i];
    $nomeTemporario = $_FILES["arquivo"]["tmp_name"][$i];

    if (isset($nomeArquivo)) {
        $_SESSION['msg'] = false;

        if (!in_array(strrchr($nomeArquivo, "."), $extensoes)) {
            $_SESSION['msg'] = "A extensão do arquivo <b>" . $nomeArquivo . "</b><br> não é válida";
            header("Location: ../admin/home.php");
            exit;
        }
        if ($tamanhoArquivo > $tamanhoMaximo) {
            $_SESSION['msg'] = "O(s) arquivos excedem o tamanho permitido.";
            header("Location: ../admin/home.php");
        }

        if (isset($_SESSION['msg'])) {

            $path_parts = pathinfo($_FILES["arquivo"]["name"][$i]);
            $extension = $path_parts['extension'];
            $destino = $caminho . $nomeArquivo;
            ftp_put($conexao_ftp, $destino, $nomeTemporario, FTP_BINARY);
            if (ftp_put($conexao_ftp, $destino, $nomeTemporario, FTP_BINARY)) {

                $queryInserir = file_get_contents("sql/insertAnexo.sql");
                $queryInserir .= " VALUES('{$resultado['ID_EMPRESA']}','{$nomeArquivo}','{$myEmpresa}','{$date}','{$descricao}','{$assuntoInsert}');";
                $inserirUp = mysqli_query($conn, $queryInserir);
                $idAnexo = mysqli_insert_id($conn);

                $queryHistorico = file_get_contents("sql/insertHistorico.sql");
                $queryHistorico .= " VALUES('{$resultado['ID_CLIENTE']}','{$idAnexo}','{$date}','UPLOAD');";
                $inserirHistorico = mysqli_query($conn, $queryHistorico);
                //$resultal = $conn->query($queryHistorico) or die($conn->error);
                $testeEnvio = true;
            } else {
                $_SESSION['msg'] = "Erro para salvar o arquivo.\nTente Novamente!!!";
                header('Location: ../admin/home.php');
            }
        } else {
            header("Location: ../admin/home.php");
        }
    } else {
        $_SESSION['msg'] = "" . "<br/>";
        ftp_close($conexao_ftp);
        header("Location: ../admin/home.php");
    }
}
if ($testeEnvio == true) {
    include('email.php');

    $buffer = file_get_contents("email/enviarAnexo.php");
    $tags = array($idAnexo, $date, $nomeEmpresa, $resultado['NOME_CLIENTE'], $descricao, $contAnexo);
    $troca = array('#idAnexo#', '#date#', '#nomeEmpresa#', '#nomeCliente#', ' #descricao#', '#quantAnexo#');
    $envio = str_replace($troca, $tags, $buffer);
    $assunto = "Novo Arquivo";
    $nomeCliente = $resultado['NOME_CLIENTE'];

    if (smtpmailer($para, $nomeCliente, $de, $de_nome, $assunto, $envio)) {
        $_SESSION['msg'] = "O arquivo <b>" . $nomeArquivo . "</b><br> foi enviado com sucesso.";
        header('Location: ../admin/home.php');
    } else {
        $_SESSION['msg'] = "Erro ao enviar email";
        header('Location: ../admin/home.php');
    }
}