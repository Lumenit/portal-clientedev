<?php

session_start();
/*
  Autor: Abdiel Pereira Cordeiro
  Tela: Administrador
 */

include('conexao.php');
header('Content-Type: text/html; charset=UTF-8');

date_default_timezone_set('America/Sao_Paulo');
$date = date('Y-m-d H:i');

function gerar_senha($tamanho, $maiusculas, $minusculas, $numeros, $simbolos) {
    $ma = "ABCDEFGHIJKLMNOPQRSTUVYXWZ";
    $mi = "abcdefghijklmnopqrstuvyxwz";
    $nu = "0123456789";
    $si = "!@#$&+*";

    $senha = 0;

    if ($maiusculas) {
        $senha .= str_shuffle($ma);
    }
    if ($minusculas) {
        $senha .= str_shuffle($mi);
    }
    if ($numeros) {
        $senha .= str_shuffle($nu);
    }
    if ($simbolos) {
        $senha .= str_shuffle($si);
    }
    return substr(str_shuffle($senha), 0, $tamanho);
}

$senhaNovoUser = gerar_senha(6, true, true, true, true);
$CriptSenha = password_hash($senha, PASSWORD_DEFAULT);
$nomeClienteNovo = $_POST['NovoUserName'];
$idEmpresaNew = $_POST['empresa'];
$emailNovoUser = $_POST['NovoUserEmail'];
$acessoUser = $_POST['OpcoesAcesso'];
$para = $_SESSION['emailUsuario'];
$para2 = $emailNovoUser;

$queryValida = file_get_contents("sql/selectCliente.sql");
$queryValida .= " WHERE EMAIL_CLIENTE = '{$emailNovoUser}'";
$validaUser = mysqli_query($conn, $queryValida);
$resultado = mysqli_fetch_assoc($validaUser);
$row = mysqli_num_rows($validaUser);

if ($row == 1) {
    $_SESSION['msg'] = "Este usuário já existe!!!";
    header('Location: ../admin/relatorios.php');
} else {
    $queryInsert = file_get_contents("sql/insertCliente.sql");
    $queryInsert .= " VALUES('{$nomeClienteNovo}','{$idEmpresaNew}','{$emailNovoUser}','{$CriptSenha}','{$acessoUser}','A')";
    $newInsert = mysqli_query($conn, $queryInsert);
    $idCliente = mysqli_insert_id($conn);
    $queryHistorico = file_get_contents("sql/insertHistorico.sql");
    $queryHistorico .= " VALUES('{$_SESSION['idCliente']}','{$idCliente}','{$date}','NEW USER');";
    $insertHistorico = mysqli_query($conn, $queryHistorico);
    $_SESSION['msg'] = "Usuário cadastrado com sucesso!!!";

    if (isset($_POST['responsavel'])) {
        $SETOR = $_POST['responsavel'];
        $contResp = count($SETOR);
        for ($i = 0; $i < $contResp; $i++) {
            $nomeResp = $_POST["responsavel"][$i];
            $queryResp = file_get_contents("sql/insertResponsavel.sql");
            $queryResp .= " VALUES('{$idCliente}','{$nomeResp}','{$idEmpresaNew}')";
            $newResp = mysqli_query($conn, $queryResp);
        }
    } else {
        
    }

    if ($row == 1) {
        $_SESSION['msg'] = "Usuário não cadastrado!!!";
        header('Location: ../admin/relatorios.php');
    } else {

        $SelectnomeEmpresa = "SELECT* FROM EMPRESA WHERE ID_EMPRESA = '{$idEmpresaNew}'";
        $nomeEmpresaResul = mysqli_query($conn, $SelectnomeEmpresa);
        $resultadoEmpresa = mysqli_fetch_assoc($nomeEmpresaResul);
        $nomeEmpresa = $resultadoEmpresa['NOME_EMPRESA'];
        include('email.php');

        $buffer = file_get_contents("email/novoUser.php");
        $tags = array($nomeEmpresa, $nomeClienteNovo, $date, $emailNovoUser, $senhaNovoUser);
        $troca = array('#nomeEmpresa#', '#nomeCliente#', '#data#', '#email#', ' #senha#');
        $envio = str_replace($troca, $tags, $buffer);
        $assunto = "Novo Usuário";
        $nomeCliente = $nomeClienteNovo;
        $nomecliente = $_SESSION['nameCliente'];

        if (smtpmailer($para, $nomecliente, $de, $de_nome, $assunto, $envio) AND smtpmailer($para2, $nomeCliente, $de, $de_nome, $assunto, $envio)) {
            $_SESSION['msg'] = "Novo Usuário criado com sucesso!!!";
            header('Location: ../admin/relatorios.php');
        } else {
            $_SESSION['msg'] = "Erro ao enviar email";
            header('Location: ../admin/relatorios.php');
        }
    }
}
