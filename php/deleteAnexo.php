<?php

/*
  Author     : Abdiel Pereira Cordeiro
  Portal Cliente
 */
session_start();
include('conexao.php');
include('conectFTP.php');

date_default_timezone_set('America/Sao_Paulo');
$date = date('Y-m-d H:i');

$pasta = "/var/www/html/dev.portal/anexoPortal/";
$testeArquivo = false;
$cont = 0;
$diretorio = dir($pasta);

$diretorio1 = scandir($pasta);
$num = count($diretorio);

$queryInserir = file_get_contents("sql/insertExclusao.sql");
$queryInserir .= " VALUES ('{$_SESSION['idCliente']}','{$date}','{$num}');";
$conect2 = mysqli_query($conn, $queryExclusao);
$id = mysqli_insert_id($conn);

$queryHistorico = file_get_contents("sql/insertHistorico.sql");
$queryHistorico .= " VALUES('{$_SESSION['idCliente']}','{$id}','{$date}','DELETE');";
$insertHistorico = mysqli_query($conn, $queryHistorico);

while ($arquivo = $diretorio->read()) {
    if (($arquivo != '.') && ($arquivo != '..')) {
        $arquivos = $pasta . $arquivo;
        ftp_delete($conexao_ftp, $arquivos);
        $cont = $cont + 1;
        $testeArquivo = true;
        $queryaux = "INSERT INTO AUX_EXCLUSAO(ID_EXCLUSAO,NOME_ANEXO) VALUES('{$id}','{$arquivo}')";
        $conect3 = mysqli_query($conn, $queryaux);
    }
}

if ($testeArquivo == true) {
    $querytrucanteAnexo = "TRUNCATE TABLE ANEXO;";
    $conect = mysqli_query($conn, $querytrucanteAnexo);

    $querytrucanteDescricao = "TRUNCATE TABLE DESCRICAO;";
    $conect1 = mysqli_query($conn, $querytrucanteDescricao);

    $_SESSION['msg'] = 'Todos os arquivos foram deletados com sucesso!!';
    $diretorio->close();
    header('Location: ../admin/home.php');
} else {
    $_SESSION['msg'] = 'Não foi possivel apagar os arquivos!!';
    header('Location: ../admin/home.php');
}
