/*
  Author     : Abdiel Pereira Cordeiro
  Portal Cliente
 */

Para que a atualização seja feita com sucesso deve se alterar as pastas de apontamento e o apontamento do banco neste arquivos a baixo:

------------------------ Quando for realizar as alterações deve mudar o arquivo index.php na raiz para index_old.php e o arquivo reparo.php para index.php
->Banco
    *conexão.php --- Alterar o banco 

-> Pasta do serviço FTP
    *deletarAnexo.php   --- alterar a pasta 
    *excluirUni.php     --- alterar a pasta
    *upload.php         --- alterar a pasta
    *uploadADM.php      --- alterar a pasta
    ->Alterar nas dos Usuários também.
        *admin/home.php     --- Alterar no final do arquivo na lista de anexos
        *user/home.php      --- Alterar no final do arquivo na lista de anexos 
