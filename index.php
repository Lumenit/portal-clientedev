<?php session_start(); ?>﻿
<!-- 
Author     : Abdiel Pereira Cordeiro
Portal
-->
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Login</title>
        <link rel="stylesheet" type="text/css" href="css/login.css">
        <link rel="shortcut icon" type="imagem/png" href="imagem/iconPortal.png">
        <script  type="text/javascript" src="bootstrap/js/popper.min.js" crossorigin="anonymous"></script>
        <script type="text/javascript" src="bootstrap/js/jquery.min.js"></script>
        <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css" crossorigin="anonymous"> 
    </head>
    <body>
        <div class="row cx"></div>
        <div class="row cx2">
            <div class="col-5"></div>
            <div class="col-4">
                <div class="row">
                    <div class="col-8 retangulo_corpo">
                        <div class="row">
                            <div class="col-1"></div>
                            <div class="col-10 cabecalho">
                                <div class='row l2'></div>
                                <img id='logoHead' src='imagem/lumenBranco.png'/>
                            </div>
                            <div class="col-1"></div>
                        </div>
                        <div class="row">
                            <div class="col-12 p1">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 corpo">
                                <div class="row">
                                    <div class="col-12">
                                        <form method="post" action="php/validarAcesso.php">
                                            <div class="row">
                                                <div class="col-2">
                                                    <div class="row l5"></div>
                                                    <img src='imagem/userLogin.png'>
                                                </div>
                                                <div class="col-10 c1">
                                                    <input type="email" name="email" placeholder="Email" id="caixaDoEmail" required="required">
                                                </div>
                                            </div>
                                            <div class="row l3"></div>
                                            <div class="row">
                                                <div class="col-2">
                                                    <div class="row l5"></div>
                                                    <img src='imagem/passLogin.png'>
                                                </div>
                                                <div class="col-10 c1">
                                                    <input type="password" name="senha" placeholder="Senha" id="senhaDoEmail" required="required">
                                                </div>
                                            </div>
                                            <div class="row l2"></div>
                                            <div class="row mensgem">
                                                <div class="col-4">
                                                    <?php
                                                    if (isset($_SESSION['msg'])) {
                                                        echo $_SESSION['msg'];
                                                        unset($_SESSION['msg']);
                                                    }
                                                    ?>
                                                </div>
                                                <div class="col-2"></div>
                                                <div class="col-6">
                                                    <button type="button" class="btn btn-trocarSenha" data-toggle="modal" data-target="#modalSenhaLogin">
                                                        Esqueceu a senha?
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-2"></div>
                                                <div class="col-8">
                                                    <button type="submit" name="btnEntrar" class="btnEntrar">Entrar</button>
                                                </div>
                                                <div class="col-2"></div> 
                                            </div>
                                        </form>
                                        <div class="modal fade" id="modalSenhaLogin" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Redifir Senha</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <form method="post" action="php/trocarSenhaLogin.php">
                                                        <div class="modal-body">
                                                            <label id="fontesTrocar">Digite seu email</label>
                                                            <input type="email" name="emailTrocarSenha" placeholder="Email" id="emailSenha" required="required"> 
                                                            <span id="mensagemRodapeSenha">                                                                         
                                                                Sua nova senha sera enviada por email!!
                                                            </span>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="submit" title="Confirma" class="btn btn-primary">Confirmar</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row l4"></div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="row">
                                            <div class="col-3"></div>
                                            <div class="col-8">
                                                <span id="mensagemRodape">
                                                    Não tem uma conta?
                                                </span>
                                                <div class="col-1"></div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class='col-1'></div>
                                            <div class="col-10">
                                                <span id="mensagemRodape">
                                                    Entra em contato no, ti@lumenit.com
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-4"></div>
        </div>
    </body>
</html>