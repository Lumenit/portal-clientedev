<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Site em Manutenção</title>
        <link rel="shortcut icon" type="imagem/png" href="imagem/icone.png">
        <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css" crossorigin="anonymous"> 
        <script type="text/javascript" src="bootstrap/js/jquery.min.js"></script>       
        <style type="text/css">
            @font-face {
                font-family: exotic;
                src: url(fontes/exotic.otf );
                font-family: American_Captain;
                src: url(fontes/American_Captain.otf);
                font-family: Disko_OT;
                src: url(fontes/Disko_OT.otf);
            }
            .l1{
                height: 100px;
                max-width: 100%;
            }
            .l2{
                max-width: 100%;
            }
            .l3{
                max-width: 100%;
            }
            .windows{
                max-width: 100%;
            }
            #title{
                font-family: Disko_OT;
                font-size: 40px;
                font-weight: bold;
            }
            #text{
                font-family: exotic;
                font-size: 20px;
            }
        </style>
    </head>
    <body>
        <div class="row l1"></div>
        <div class="row l2">
            <div class="col-2"></div>
            <div class="col-3">
                <img src="imagem/reparo.png">
            </div>
            <div class="col-4">
                <div class="row">
                    <div class="col-12">
                        <label id="title">Site em Manutenção</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <p id="text">
                            Estamos realizando alguns reparos em nosso aplicativo para melhor atende-lo
                            <br>Desculpa pelo incomodo voltamos em instantes !!!
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row l3">
            <div class="col-5"></div>
            <div class="col-4">
                <img src="imagem/lumen.png">
            </div>
        </div>
    </body>
</html>