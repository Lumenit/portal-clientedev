/* global $_SESSION */

$(document).ready(function () {// Janelas modais de Trocar senha e historico

    $('a[name=modal]').click(function (e) {
        e.preventDefault();

        var id = $(this).attr('href');

        var maskHeight = $(document).height();
        var maskWidth = $(window).width();

        $('#mask').css({'width': maskWidth, 'height': maskHeight});

        $('#mask').fadeIn(1000);
        $('#mask').fadeTo("slow", 0.8);

        //Get the window height and width
        var winH = $(window).height();
        var winW = $(window).width();

        $(id).css('top', winH / 2 - $(id).height() / 2);
        $(id).css('left', winW / 2 - $(id).width() / 2);

        $(id).fadeIn(2000);

    });

    $('.window .close').click(function (e) {
        e.preventDefault();

        $('#mask').hide();
        $('.window').hide();
    });

    $('#mask').click(function () {
        $(this).hide();
        $('.window').hide();
    });

});

//Validação da troca de senha para ver se estão iguais
function validaSenha(input) {
    if (input.value !== document.getElementById('novaSenha').value) {
        input.setCustomValidity('Repita a senha corretamente');
    } else {
        input.setCustomValidity('');
    }
}
function makeFileList() {
    var input = document.getElementById("arquivo");
    var ul = document.getElementById("resultado");
    var ull = document.getElementById("resultado2");
    var ul2 = document.getElementById("resultado1");
    var conta = 0;

    while (ul.hasChildNodes()) {
        ul.removeChild(ul.firstChild);
        ull.removeChild(ull.firstChild);
        ul2.removeChild(ul2.firstChild);
    }
    for (var i = 0; i < input.files.length; i++) {
        var li = document.createElement("li");
        var lii = document.createElement("lii");
        li.innerHTML = input.files[i].name;

        if (input.files[i].size > 1073741824) {
            var conta = (conta + (((input.files[i].size / 1024) / 1024) / 1024));
        }else{
            var conta = (conta + ((input.files[i].size / 1024) / 1024));
        }


        ul.appendChild(li);
        ull.appendChild(lii);
    }
    lii.innerHTML = parseFloat(conta.toFixed(1));

    var li2 = document.createElement("li");
    var li3 = document.createElement("lii");

    li2.innerHTML = i + " Arquivo selecionado";
    if (conta > 1) {
        li3.innerHTML = i + "Gb";
    }else{
        li3.innerHTML = i + "Mb";
    }


    ul2.appendChild(li2);
    ull.appendChild(li3);

    if (!ul.hasChildNodes()) {
        var li = document.createElement("li");
        li.innerHTML = 'Escolha';

        ul.appendChild(li);
    }
}

function chPage() {
    var id = $('#inputGroupSelect01').val();
    $('#seg').load("/dev.portal/user/buscarNome.php?id=" + id);
}
function carregarTSenha() {
    $("#janelSenha").load('/dev.portal/janelas/trocarsenha.php');
}
