<?php
require_once('../php/restrito.php');
include('../php/timeout.php');
/*
  Author     : Abdiel Pereira Cordeiro
  Tela: Usuário Portal
 */
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <title>Portal</title>
        <link rel="stylesheet"  type="text/css" href="css/css.css">
        <script type="text/javascript" src="js/javascript.js"></script>
        <script  type="text/javascript" src="../bootstrap/js/popper.min.js" crossorigin="anonymous"></script>
        <script type="text/javascript" src="../bootstrap/js/jquery.min.js"></script>
        <script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css" crossorigin="anonymous"> 
        <script type="text/javascript" src="../bootstrap/js/jquery-2.1.3.min.js"></script>
        <link rel = "shortcut icon" type = "imagem/png" href = "../imagem/iconPortal.png"/>
    </head>
    <body>
        <script type="text/javascript" src="js/wz_tooltip.js"></script>
        <div class="row linhaCabecario"></div>
        <div class="row cabecario">
            <div class="col-2 p1">
                <img src="../imagem/lumen.png" id="logoLumen">
            </div>
            <div class="col-3"></div>
            <div class="col-2">
                <div class="row l10"></div>
                <div class="row">
                    <div class="col-2">
                        <img src="../imagem/Usuario.png" id="iconeUsuario"/>
                    </div>
                    <div class="col-10">
                        <div id="infosUser"><?php echo $_SESSION['nameCliente'] ?></div>
                    </div>
                </div>
            </div>
            <div class="col-2"></div>
            <div class="col-2">
                <div class="row l5"></div>
                <div class="row">
                    <div class="col-8">
                        <a onclick="carregarTSenha();" class="btn" data-target="#modalSenha" data-toggle="modal" href="#modalSenha" id="trocarSenha">Trocar Senha</a>
                    </div>
                    <div class="col-2">
                        <form method="post" action="../php/logout.php" name="Sair" >
                            <button id="sair" type="submit" class="btn btn-secondary">Sair</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-1">
                <div class="row l11"></div>
                <div class="row">
                    <div class="col-12">
                        <label id="versao"><?php echo"{$_SESSION['versaoSistem']}"; ?></label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="modal fade" id="modalSenha" tabindex="-1" role="dialog" aria-labelledby="modalSenha" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div id="janelSenha"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row l3"></div>
        <div class="row l2"></div>
        <div class="row margin">
            <div class="col-4">
                <div class="row">
                    <div class="col-10 upload">
                        <div id="backgroundUpload">
                            <div class="row">
                                <div class="col-8"></div>
                                <div class="col-2 l4">
                                    <img src="../imagem/duvida.png" id="duvi1" onmouseover="Tip('Extenssões validas: .doc, .pdf, .docx, .rar, .zip, .xlsx, .xls, .csv, .exe, .ppt, .pptx')" onmouseout="UnTip()" />
                                </div>
                            </div>
                            <form name="upload" enctype="multipart/form-data" method="post" action="../php/upload.php">
                                <div class="row">
                                    <div class="col-10 p4">
                                        <div class="row l5"></div>
                                        <table>
                                            <tr>
                                                <td id="tableUp">
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text inp" id="inputGroupFileAddon01"><img src="../imagem/upload.png"></span>
                                                        </div>
                                                        <div class="custom-file">
                                                            <input type="hidden">
                                                            <input type="file" aria-describedby="inputGroupFileAddon01" id="arquivo" class="custom-file-input" name="arquivo[]" multiple="multiple"  onChange="makeFileList();"/>
                                                            <label class="custom-file-label inp" for="inputGroupFile01">Escolha um Arquivo</label>
                                                        </div>
                                                    </div>     
                                                </td>                                                
                                                <td>
                                                    <div class="tableUpFake" id="resultado1"> </div>
                                                    <div class="tableUpFake" id="resultado2"> </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="background:#DCDCDC;">
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <label class="input-group-text" for="inputGroupSelect01">Setor</label>
                                                        </div>
                                                        <select id="inputGroupSelect01" name="OpcoesEmpresa" class="custom-select OpcoesCliente" onchange="chPage();" required="required">
                                                            <option value="" disabled selected>Selecione</option>
                                                            <?php
                                                            include('../php/conexao.php');
                                                            $queryClientes = "SELECT DISTINCT EMPRESA.NOME_EMPRESA, EMPRESA.ID_EMPRESA FROM SETOR INNER JOIN EMPRESA ON EMPRESA.NOME_EMPRESA = SETOR.NOME_EMPRESA WHERE SETOR.ID_CLIENTE = '{$_SESSION['idCliente']}';";
                                                            $result = mysqli_query($conn, $queryClientes);
                                                            while ($rowEmpresa = mysqli_fetch_assoc($result)) {
                                                                echo" <option value=\"{$rowEmpresa['ID_EMPRESA']}\">{$rowEmpresa['NOME_EMPRESA']}</option>";
                                                            }
                                                            $result->close();
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div id="seg">
                                                        <div class="input-group mb-3">
                                                            <div class="input-group-prepend">
                                                                <label class="input-group-text" for="inputGroupSelect01">Email</label>
                                                            </div>
                                                            <select id="inputGroupSelect01" name="OpcoesCliente" class="custom-select OpcoesCliente" onchange="chPage();" required="required">
                                                                <option value="" disabled selected>Selecione</option>

                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" id="basic-addon1">Titulo</span>
                                                        </div>
                                                        <input type="text" name="titulo" class="form-control" placeholder="Titulo" aria-label="Username" required="required">
                                                    </div>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">Mensagem</span>
                                                        </div>
                                                        <textarea maxlength="1000" class="form-control" name="assunto" aria-label="With textarea"></textarea>
                                                    </div>
                                                    <button class="bntEnviar" name="enviar" type="submit" value="Enviar"><img src="../imagem/seta.png"></button>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-1"></div>
                                    <div class="col-10">
                                        <fieldset style="border:none;">
                                            <table border="0" width="100%">
                                                <tr>
                                                    <td>
                                                        <div id="resultado" style="width: 400px;">Capacidade maxima de 1Gb</div>
                                                    </td>
                                                </tr>
                                            </table> 
                                        </fieldset>
                                    </div>
                                </div>
                            </form>  
                        </div>
                    </div>
                </div>
                <div class="row l3"></div>
                <div class="row">
                    <div class="col-2"></div>
                    <div class="col-6" id="erroTrocaSenha">
                        <?php
                        if (isset($_SESSION['msgAnexar'])) {
                            echo"<div class=\"balaoErro\">";
                            echo $_SESSION['msgAnexar'];
                            unset($_SESSION['msgAnexar']);
                            echo"</div>";
                        }
                        if (isset($_SESSION['msgTrocaSenha'])) {
                            echo"<div class=\"balaoErro\">";
                            echo $_SESSION['msgTrocaSenha'];
                            unset($_SESSION['msgTrocaSenha']);
                            echo"</div>";
                        }
                        if (isset($_SESSION['erroLimpar'])) {
                            echo"<div class=\"balaoErro\">";
                            echo $_SESSION['erroLimpar'];
                            unset($_SESSION['erroLimpar']);
                            echo"</div>";
                        }
                        if (isset($_SESSION['msgApagarArq'])) {
                            echo"<div class=\"balaoErro\">";
                            echo $_SESSION['msgApagarArq'];
                            unset($_SESSION['msgApagarArq']);
                            echo"</div>";
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="col-1"></div>
            <div class="col-5 margin">
                <div class="col-12" id="backgroundTela">
                    <div class="row">
                        <div class="col-3"></div>
                        <div class="col-4 titulo1">
                            <label id="tituloPage">Anexos</label>
                        </div>
                        <div class = "col-5">
                            <form action = 'home.php' method = 'post'>
                                <div class = "row">
                                    <div class = "col-8">
                                        <input type="text" class="input" id="txtBusca" name="busca" placeholder="Buscar..."/>
                                    </div>
                                    <div class = "col-2">
                                        <input type="submit" class="btnBusca input" value=""/>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 tbl1">
                            <table class="table tmTabelas">
                                <thead class="thead-dark">
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col"><img src="../imagem/anexoid.png">ANEXO</th>
                                        <th scope="col"><img src="../imagem/tituloicon.png">TITULO</th>
                                        <th scope="col"><img src="../imagem/dateicon.png">DATA</th>
                                        <th scope="col"></th>
                                        <th scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    include('../php/conexao.php');
                                    $idCliente = $_SESSION['idCliente'];
                                    $idEmpresa = $_SESSION['idEmpresa'];

                                    $queryAnexo = "SELECT* FROM consulanexouser ";
                                    $queryAnexo .= " WHERE ID_EMPRESA_REMETENTE = '{$idEmpresa}' ";
                                    $queryAnexo .= " OR ID_CLIENTE_DESTINATARIO = '{$idEmpresa}' ";
                                    $resultal = $conn->query($queryAnexo) or die($conn->error);
                                    if (isset($_POST['busca']) AND $_POST['busca'] != '') {
                                        $busca = $_POST['busca'];
                                        $queryAnexo .= " AND NOME_ANEXO LIKE '%{$busca}%'";
                                        $queryAnexo .= " OR ASSUNTO LIKE '%{$busca}%'";
                                        $queryAnexo .= " OR ID_ANEXO LIKE '%{$busca}%'";
                                    }
                                    $queryAnexo .= " ORDER BY DATA DESC";
                                    if ($resultAnexo = $conn->query($queryAnexo)) {
                                        while ($row = $resultAnexo->fetch_assoc()) {
                                            echo "<tr>"
                                            . "<td>{$row['ID_ANEXO']}</td>"
                                            . "<td id=\"td1\">{$row['NOME_ANEXO']}</td>"
                                            . "<td>{$row['ASSUNTO']}</td>"
                                            . "<td>{$row['DATA']}</td>"
                                            . "<td><a href='/anexoPortal/{$row['NOME_ANEXO']}' target='_blank'><img src='../imagem/download.png'></a></td>"
                                            . "<td><a href='../php/excluirUni.php?id={$row['ID_ANEXO']}' name=\"excluir\"><img src='../imagem/deletarUni.png'></a></td>"
                                            . "</tr>";
                                        }
                                        $resultAnexo->close();
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>